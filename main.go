package main

import (
	"database/sql"
	"fmt"

	"github.com/jackc/pgx/v5/pgtype"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/zeebo/errs"
)

func main() {
	err := do()
	if err != nil {
		println(err.Error())
	}
}

func do() error {
	db, err := sql.Open("pgx", "postgresql://127.0.0.1:5432/storj?user=storj&password=password&sslmode=disable")
	if err != nil {
		return errs.Wrap(err)
	}

	_, err = db.Exec("DROP TABLE IF EXISTS green")
	if err != nil {
		return errs.Wrap(err)
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS green (id bigint, items bigint[])")
	if err != nil {
		return errs.Wrap(err)
	}

	_, err = db.Exec("INSERT INTO green (id, items) VALUES ($1, $2)", 43, pgtype.FlatArray[int]([]int{12, 13}))
	if err != nil {
		return errs.Wrap(err)
	}

	rows, err := db.Query("SELECT id, items FROM green WHERE id = $1", 43)
	if err != nil {
		return errs.Wrap(err)
	}

	//mapp := pgtype.NewMap()

	var id int
	if !rows.Next() {
		return fmt.Errorf("no rows")
	}
	var items []int
	err = rows.Scan(&id, &items)
	if err != nil {
		return errs.Wrap(err)
	}

	fmt.Printf("%d %+v", id, items)

	return nil
}
